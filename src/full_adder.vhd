entity E_FULL_ADDER is
  port( A, B, CIN: in bit;
        S, COUT: out bit);
end E_FULL_ADDER;

architecture A_FULL_ADDER_COMBI of E_FULL_ADDER is
begin
  FULL_ADDER: process(A, B, CIN)
  begin
    COUT <= (A and B) or (A and CIN) or (B and CIN) after 10 ns; -- 5 ns for the ANDs, 5 for the ORs
    S <= (A xor B) xor CIN after 30 ns; -- 15 ns for earch xor
  end process FULL_ADDER;
end A_FULL_ADDER_COMBI;

architecture A_FULL_ADDER_LOOKUP of E_FULL_ADDER is
  signal SUMMANDS: bit_vector(2 downto 0);
begin
  SUMMANDS <= (CIN,B,A);
  FULL_ADDER: process(A, B, CIN)
  begin
    case SUMMANDS is
      when "000" => S <= '0'; COUT <= '0';
      when "001" => S <= '1'; COUT <= '0';
      when "010" => S <= '1'; COUT <= '0';
      when "011" => S <= '0'; COUT <= '1';
      when "100" => S <= '1'; COUT <= '0';
      when "101" => S <= '0'; COUT <= '1';
      when "110" => S <= '0'; COUT <= '1';
      when "111" => S <= '1'; COUT <= '1';
    end case;
  end  process FULL_ADDER;
end A_FULL_ADDER_LOOKUP;
