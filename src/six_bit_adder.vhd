entity E_SIX_BIT_ADDER is
  port( A, B : in bit_vector(5 downto 0);
        CIN : in bit;
        S : out bit_vector(5 downto 0);
        COUT: out bit);
end E_SIX_BIT_ADDER;

architecture A_SIX_BIT_ADDER of E_SIX_BIT_ADDER is
  component E_FULL_ADDER
    port( A, B, CIN: in bit;
          S, COUT: out bit);
  end component;
  signal CARRIES : bit_vector(4 downto 0);
begin
  SIX_ADDERS: for i in 0 to 5 generate
    A0: IF (i=0) generate
      add0 : E_FULL_ADDER port map (
        A => A(i),
        B => B(i),
        CIN => CIN,
        S => S(i),
        COUT => CARRIES(i)
      );
    end generate A0;
    A1_4: IF ((0<i) AND (i<5)) generate
      add0 : E_FULL_ADDER port map (
        A => A(i),
        B => B(i),
        CIN => CARRIES(i-1),
        S => S(i),
        COUT => CARRIES(i)
      );
    end generate A1_4;
    A5: IF (i=5) generate
      add0 : E_FULL_ADDER port map (
        A => A(i),
        B => B(i),
        CIN => CARRIES(i-1),
        S => S(i),
        COUT => COUT
      );
    end generate A5;
  end generate SIX_ADDERS;

  SIX_BIT_ADDER: process(A, B, CIN)
  begin
  end process SIX_BIT_ADDER;
end A_SIX_BIT_ADDER;
