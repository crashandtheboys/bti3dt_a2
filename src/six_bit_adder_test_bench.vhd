entity E_SIX_BIT_ADDER_TEST_BENCH is
end E_SIX_BIT_ADDER_TEST_BENCH;

architecture A_SIX_BIT_ADDER_TEST_BENCH of E_SIX_BIT_ADDER_TEST_BENCH is
  --  Declaration of the component that will be instantiated.
  component E_SIX_BIT_ADDER
    port( A, B : in bit_vector(5 downto 0);
          CIN : in bit;
          S : out bit_vector(5 downto 0);
          COUT: out bit);
  end component;
  signal A, B : bit_vector(5 downto 0);
  signal CIN : bit;
  signal S : bit_vector(5 downto 0);
  signal COUT: bit;
begin
  --  Component instantiation.
  six_bit_adder: E_SIX_BIT_ADDER port map (
    A => A,
    B => B,
    CIN => CIN,
    S => S,
    COUT => COUT
  );
  -- Stimulus process
  stim_proc: process
  begin
    A <= "000000"; B <= "000000"; CIN <= '0'; wait for 1000 ns;
    A <= "010101"; B <= "101010"; CIN <= '0'; wait for 1000 ns;
    A <= "010101"; B <= "101010"; CIN <= '1'; wait for 1000 ns;
    wait for 1000 ns;
    wait;
  end process;
end A_SIX_BIT_ADDER_TEST_BENCH;
