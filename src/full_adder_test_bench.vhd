entity E_FULL_ADDER_TEST_BENCH is
end E_FULL_ADDER_TEST_BENCH;

architecture A_FULL_ADDER_TEST_BENCH of E_FULL_ADDER_TEST_BENCH is
  --  Declaration of the component that will be instantiated.
  component E_FULL_ADDER
    port( A, B, CIN: in bit;
          S, COUT: out bit);
  end component;
  --  Specifies which entity is bound with the component.
  -- for adder: E_FULL_ADDER use entity work.E_FULL_ADDER;
  signal A, B, CIN, S, COUT: bit;
begin
  --  Component instantiation.
  adder: E_FULL_ADDER port map (
    A => A,
    B => B,
    CIN => CIN,
    S => S,
    COUT => COUT
  );
  -- Stimulus process
  stim_proc: process
  begin
    -- use grey code to stimulate inputs
    A <= '1'; B <= '0'; CIN <= '0'; wait for 500 ns;
    A <= '1'; B <= '1'; CIN <= '0'; wait for 500 ns;
    A <= '0'; B <= '1'; CIN <= '0'; wait for 500 ns;
    A <= '0'; B <= '1'; CIN <= '1'; wait for 500 ns;
    A <= '1'; B <= '1'; CIN <= '1'; wait for 500 ns;
    A <= '1'; B <= '0'; CIN <= '1'; wait for 500 ns;
    A <= '1'; B <= '0'; CIN <= '0'; wait for 500 ns;
    A <= '0'; B <= '0'; CIN <= '0'; wait for 500 ns;
    wait for 500 ns;
    wait;
  end process;
end A_FULL_ADDER_TEST_BENCH;
